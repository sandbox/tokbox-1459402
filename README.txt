
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Current Maintainer: Jonathan Mumm <mumm@tokbox.com>

OpenTok TokShow is a drupal module for creating and customizing TokBox's TokShow
application to live on your Drupal website.

Visit here to see TokShow: http://www.tokbox.com/opentok/plugnplay#TokShow

INSTALLATION
------------

Once the module is installed, visit admin/config/tokshow to configure. You
will need to get an API key from TokBox in order to generate your TokShow. To
do that, follow these two steps:

1) Go to this URL to get an OpenTok sandbox key:
http://www.tokbox.com/opentok/api/tools/js/apikey

2) You will receive an email with your sandbox key. Then go to this URL to
activate your key on OpenTok's production environment:
http://www.tokbox.com/opentok/api/tools/js/launch

This will give you an API key and secret that you should enter on the
configuration page.
